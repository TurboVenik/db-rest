package ru.mephi.bublechart.web.dto;

import lombok.Data;

@Data
public class BubbleDto {

    private Integer projectId;
    private String projectName;
    private AttributeDto x;
    private AttributeDto y;
    private AttributeDto r;
    private AttributeDto t;
}
